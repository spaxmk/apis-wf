﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Text;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Forms;
using MessageBox = System.Windows.Forms.MessageBox;

namespace APIs
{
    public partial class APIs : Form
    {
        
        public APIs()
        {
            InitializeComponent();
            System.Windows.Forms.Timer timer1 = new System.Windows.Forms.Timer();
            timer1.Tick += new System.EventHandler(timer1_Tick_1);
            timer1.Start(); 
            
        }

        private async void btnGetAll_Click(object sender, EventArgs e)
        {
            var responce = await RestHelper.GetAll();
            txtResoince.Text = RestHelper.BeautifyJson(responce);
        }

        private async void btnPost_Click(object sender, EventArgs e)
        {
            var responce = await RestHelper.Post(txtIme.Text, txtMesto.Text, txtGodina.Text);
            txtResoince.Text = RestHelper.BeautifyJson(responce);
            txtEmail.Text = "Email:";
            txtIme.Text = "Ime:";
            txtMesto.Text = "Mesto:";
            txtGodina.Text = "Godina:";
        }

        private async void btnGet_Click(object sender, EventArgs e)
        {
            var responce = await RestHelper.GetById(txtID.Text);
            txtResoince.Text = RestHelper.BeautifyJson(responce);
        }

        private async void btnPut_Click(object sender, EventArgs e)
        {
            var responce = await RestHelper.Put(txtID.Text, txtIme.Text, txtMesto.Text, txtGodina.Text);
            txtResoince.Text = RestHelper.BeautifyJson(responce);
            //txtEmail.Text = "Email:";
            txtIme.Text = "Ime:";
            txtMesto.Text = "Mesto:";
            txtGodina.Text = "Godina:";
        }

        private async void btnDelete_Click(object sender, EventArgs e)
        {
            var responce = await RestHelper.Delete(txtID.Text);
            txtResoince.Text = responce;
            txtID.Text = "ID:";
        }
        private void txtID_Enter(object sender, EventArgs e)
        {
            if (txtID.Text.Equals("ID:"))
                txtID.Clear();
            else
                txtID.SelectAll();
        }
        private void txtID_Leave(object sender, EventArgs e)
        {
            if(txtID.Text == "")
                txtID.Text = "ID:";
        }

        private void txtEmail_Enter(object sender, EventArgs e)
        {
            txtEmail.Clear();
        }

        private void txtEmail_Leave(object sender, EventArgs e)
        {
            if (txtEmail.Text == "")
                txtEmail.Text = "Email:";
        }

        private void txtIme_Enter(object sender, EventArgs e)
        {
            txtIme.Clear(); ;
        }
        private void txtIme_Leave(object sender, EventArgs e)
        {
            if (txtIme.Text == "")
                txtIme.Text = "Ime:";
        }

        private void txtMesto_Enter(object sender, EventArgs e)
        {
            txtMesto.Clear();
        }
        private void txtMesto_Leave(object sender, EventArgs e)
        {
            if (txtMesto.Text == "")
                txtMesto.Text = "Mesto:";
        }
        private void txtGodina_Enter(object sender, EventArgs e)
        {
            txtGodina.Clear();
        }
        private void txtGodina_Leave(object sender, EventArgs e)
        {
            if (txtGodina.Text == "")
                txtGodina.Text = "Godina:";
        }

        //Check network
        private void CheckConn()
        {
            bool stats = false;
            try
            {
                if (System.Net.NetworkInformation.NetworkInterface.GetIsNetworkAvailable() != true)
                    NoConn();
                else
                    Conn();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
            }
        }

        private void NoConn()
        {
            if(btnGetAll.Enabled != false || btnGet.Enabled != false || btnPost.Enabled != false || btnPut.Enabled != false || btnDelete.Enabled != false)
            {
                txtID.Enabled = false;
                txtEmail.Enabled = false;
                txtIme.Enabled = false;
                txtMesto.Enabled = false;
                txtGodina.Enabled = false;
                btnGetAll.Enabled = false;
                btnGet.Enabled = false;
                btnPost.Enabled = false;
                btnPut.Enabled = false;
                btnDelete.Enabled = false;
                lbl1.Text = "Connection problem!";
                MessageBox.Show("Connection problem! Check your network connection!");
            }

        }
        private void Conn()
        {
            if (btnGetAll.Enabled == false || btnGet.Enabled == false || btnPost.Enabled == false || btnPut.Enabled == false || btnDelete.Enabled == false)
            {
                txtID.Enabled = true;
                txtEmail.Enabled = true;
                txtIme.Enabled = true;
                txtMesto.Enabled = true;
                txtGodina.Enabled = true;
                btnGetAll.Enabled = true;
                btnGet.Enabled = true;
                btnPost.Enabled = true;
                btnPut.Enabled = true;
                btnDelete.Enabled = true;
                lbl1.Text = "";
                MessageBox.Show("Connected!");
            }
        }

        private void timer1_Tick_1(object sender, EventArgs e)
        {
            CheckConn();
        }
    }
}
